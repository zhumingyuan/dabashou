

App({
  api: require("utils/api.js"),
  onLaunch: function() {
    var e = this,
      t = wx.getStorageSync("logs") || [];
    t.unshift(Date.now()), wx.setStorageSync("logs", t), wx.setStorageSync("isLogin", 0),
      wx.getSystemInfo({
        success: function(t) {
          e.globalData.navHeight = t.statusBarHeight + 40, e.globalData.time = 3e3, e.globalData.device = t;
          var n = {
            x: t.windowWidth - 70,
            y: t.windowHeight - 110
          };
          wx.setStorageSync("kefu", n), e.globalData.fontSize = t.fontSizeSetting;
        },
        fail: function(e) {
          console.log(e);
        }
      }), wx.getSetting({
        success: function(t) {
          t.authSetting["scope.userInfo"] && wx.getUserInfo({
            lang: "zh_CN",
            success: function(n) {
              wx.request({
                url: "https://kol.zhongjie.com/api/saveInfo",
                data: n,
                method: "POST",
                header: {
                  "content-type": "application/json",
                  token: wx.getStorageSync("token")
                },
                success: function(e) {},
                fail: function(e) {}
              }), e.globalData.userInfo = t.userInfo, e.userInfoReadyCallback && e.userInfoReadyCallback(t);
            }
          });
        }
      });
  },
  onShow: function() {},
  onError: function(e) {
    this.api.getError(e).then(function(t) {
      console.log(e);
    }).catch();
  },
  globalData: {
    userInfo: null,
    qtel: "点击填写取货信息",
    qname: "",
    qaddress: "从哪里取货？",
    qdetail: "",
    stel: "点击填写收货信息",
    sname: "",
    saddress: "要送到哪里？",
    sdetail: ""
  },
  moveEnd: function(e, t, n) {
    var o = wx.getStorageSync("kefu");
    return {
      y: e.changedTouches[0].pageY > 100 ? e.changedTouches[0].pageY - 35 : t + 20,
      x: e.changedTouches[0].pageX < n / 2 ? 10 : o.x
    };
  }
});