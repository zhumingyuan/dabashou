function t(t, r) {
  if ("" != t)
    for (var i in t) "string" == typeof t[i] && (t[i] = t[i].trim());
  if ("" == t) t = {}
  return new Promise(function(i, s) {
    t.token = wx.getStorageSync("token")
    wx.request({
      url: a.api_url + r,
      data: t,
      method: "POST",
      header: {
        "content-type": "application/json",
        // token: wx.getStorageSync("token")
      },
      success: function(t) {
        "fail" == t.data.state ? (void 0 != t.data.msg && void 0 != t.data.errorNum ? (e(t.data.msg + t.data.errorNum),
          n(t.data.msg.substr(0, 15), "none", 2e3)) : (e(r + "此接口报错 ：" + JSON.stringify(t)),
          n("error", "none", 2e3)), "" != t.data.code && 2 == t.data.code ? o() : i(t)) : i(t);
      },
      fail: s
    });
  });
}

function e(t) {
  return new Promise(function(e, n) {
    var o = getCurrentPages(),
      i = wx.getSystemInfoSync(),
      s = {
        level: "error",
        time: parseInt(new Date().getTime() / 1e3),
        type: o.length < 1 ? "2" : "1",
        path: o.length < 1 ? "app.js" : o[o.length - 1].route,
        msg: "object" === (void 0 === t ? "undefined" : r(t)) ? t : String(t),
        systemInfo: JSON.stringify(i)
      };
    wx.request({
      url: a.api_url + "/sys/frontLog",
      data: s,
      method: "POST",
      header: {
        "content-type": "application/json",
        token: wx.getStorageSync("token")
      },
      success: e,
      fail: n
    });
  });
}

function n(t, e, n) {
  wx.showToast({
    title: t,
    icon: e,
    duration: n
  });
}

function o() {
  wx.login({
    success: function (n) {
      wx.setStorageSync("code", n.code);
      var s = {
        code: n.code,
        inviterGuid: void 0 == t ? "" : t
      };
      t(s, "/api/login").then(function (n) {
        console.log(111)
        console.log(n)
        if (1 == n.data.status) wx.setStorageSync("token", n.data.token), wx.setStorageSync("isLogin", 1),
          console.log(wx.getStorageSync("userInfo")), "" != wx.getStorageSync("userInfo") && "" != wx.getStorageSync("token") || i.showLayer();
      }).catch(function (t) {
        e(t).then(function (t) { }).catch();
      });
    }
  });
}

function x(t) {
  return "https://kol.zhongjie.com/" + t
}

function arr(t) {
  var arry = t.split(",")
  for (var i in arry) arry[i]=x(arry[i])
  return arry
}
var r = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
    return typeof t;
  } : function(t) {
    return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
  },
  a = require("config.js");

module.exports = {
  post: t,
  getError: e,
  userLogin: o,
  showTips: n,
  addBase: x,
  toArr: arr,
  saveFormId: function(t) {
    var e = {
      formId: t
    };
    wx.request({
      url: a.api_url + "/talent/saveFormId",
      data: e,
      method: "POST",
      header: {
        "content-type": "application/json",
        token: wx.getStorageSync("token")
      },
      success: function(t) {},
      fail: function(t) {}
    });
  }
};