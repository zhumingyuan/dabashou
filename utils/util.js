function e(e) {
  return e < 10 ? "0" + e : e;
}

var t = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
    return typeof e;
  } : function(e) {
    return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
  },
  u = function(e) {
    return (e = e.toString())[1] ? e : "0" + e;
  };

module.exports = {
  formatNumber: u,
};