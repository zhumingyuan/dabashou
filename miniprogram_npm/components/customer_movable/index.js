var t = getApp();

Component({
    properties: {
        x: Number,
        y: Number
    },
    data: {},
    methods: {
        moveEnd: function(e) {
            var a = t.moveEnd(e, t.globalData.navHeight, t.globalData.device.windowWidth);
            this.setData({
                x: a.x,
                y: a.y
            });
        }
    }
});