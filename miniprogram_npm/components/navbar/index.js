var a = getApp();

Component({
    properties: {
        pageName: String,
        showNav: Boolean,
        showHome: Boolean,
        color: String
    },
    data: {
        navH: a.globalData.navHeight,
        fontsize: a.globalData.fontSize
    },
    methods: {
        navBack: function() {
            wx.navigateBack({
                delta: 1
            });
        },
        toIndex: function() {
            wx.switchTab({
                url: "/pages/index/index"
            });
        }
    }
});