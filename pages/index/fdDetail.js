function a(a, t, e) {
  return t in a ? Object.defineProperty(a, t, {
    value: e,
    enumerable: !0,
    configurable: !0,
    writable: !0
  }) : a[t] = e, a;
}

var t = getApp(),
  e = require("../../utils/util.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    navH: t.globalData.navHeight,
    u_name: "",
    u_phone: "",
    u_address: "",
    u_detail: "",
    u_total: "",
    shibie: 0,
    title: 0
  },
  onChangeAddress: function() {
    var that = this;
    //获取用户当前的授权状态
    wx.getSetting({
      success(res) {
        //若用户没有授权地理位置
        if (!res.authSetting['scope.userLocation']) {
          //在调用需授权 API 之前，提前向用户发起授权请求
          wx.authorize({
            scope: 'scope.userLocation',
            //用户同意授权
            success() {
              // 用户已经同意小程序使用地理位置，后续调用 wx.getLocation 接口不会弹窗询问
            },
            // 用户不同意授权
            fail() {
              wx.showModal({
                title: '提示',
                content: '此功能需获取位置信息，请授权',
                success: function(res) {
                  if (res.confirm == false) {
                    return false;
                  }
                  wx.openSetting({
                    success(res) {
                      //如果再次拒绝则返回页面并提示
                      if (!res.authSetting['scope.userLocation']) {
                        t.api.showTips("此功能需获取位置信息，请重新设置", "none", 3e3)
                      } else {
                        //允许授权，调用地图
                        that.onLoad()
                      }
                    }
                  })
                }
              })
            }
          })
        } else {
          that.chooseLocal()
        }
      }
    })
  },
  chooseLocal() {
    var that = this
    wx.chooseLocation({
      success: function(res) {
        console.log(res)
        that.setData({
          u_address: res.name
        });

      },
      fail: function(err) {
        console.log(err)
        if (err.errMsg.indexOf("deny") >= 0) {
          t.api.showTips("未授权地址无法使用", "none", 3e3)
        }
      }
    });
  },
  iptChange(e) {
    var that = this
    var v = e.currentTarget.dataset.ipt
    that.setData({
      [v]: e.detail.value
    })
  },
  areChange(e) {
    var that = this
    e.detail.value == "" ? that.setData({
      shibie: 0
    }) : that.setData({
      shibie: 1,
      u_total: e.detail.value
    })
  },
  del(e) {
    var that = this
    that.setData({
      u_total: "",
      shibie: 0,
    })
  },
  autoComplete() {
    var that = this
    var text = that.data.u_total
    text = text.replace(/(^\s*)|(\s*$)/g, "");
    if (!that.data.shibie) {
      return
    } else {
      var regx = /(1[3|4|5|7|8][\d]{9}|0[\d]{2,3}-[\d]{7,8}|400[-]?[\d]{3}[-]?[\d]{4})/g;
      var phone_num = text.match(regx);
      if (phone_num != null) {
        var phone = text.indexOf(phone_num[0]);
      }
      var name = text.indexOf("姓名:");
      if (name >= 0) {
        var phone = text.indexOf("电话:"),
          address = text.indexOf("地址:");
        var u_name = text.substring(name + 3, phone),
          u_phone = text.substring(phone + 3, address),
          u_address = text.substring(address + 3, text.length);
        that.setData({
          u_name: u_name,
          u_phone: u_phone,
          u_address: u_address
        })
      } else if (phone >= 0) {
        var u_name = text.substring(0, phone),
          u_phone = text.substring(phone, phone + 11),
          u_address = text.substring(phone + 11, text.length);
        that.setData({
          u_name: u_name,
          u_phone: u_phone,
          u_address: u_address
        })
      } else {
        t.api.showTips("识别失败！请手动填写", "none", 3e3)
        that.setData({
          u_name: '',
          u_phone: '',
          u_address: ''
        })
        return;
      }
    }
  },
  save() {
    var that = this
    var regx = /(1[3|4|5|7|8][\d]{9}|0[\d]{2,3}-[\d]{7,8}|400[-]?[\d]{3}[-]?[\d]{4})/g;
    if (that.data.u_address == "") {
      t.api.showTips("请填写地址", "none", 3e3)
      return
    }
    if (that.data.title) {
      if (that.data.u_phone == "" || !that.data.u_phone.match(regx)) {
        t.api.showTips("请填写正确的手机号", "none", 3e3)
        return
      }
    } else {
      if (that.data.u_phone == "") {
        t.api.showTips("请填写取件码", "none", 3e3)
        return
      }
    }
    if (that.data.u_name == "") {
      t.api.showTips("请填写联系人", "none", 3e3)
      return
    }

    var address,
      detail,
      tel,
      name,
      qu = that.data.title
    qu ? (address = "saddress", tel = "stel", name = "sname", detail = "sdetail") : (address = "qaddress", tel = "qtel", name = "qname", detail = "qdetail")
    t.globalData[name] = that.data.u_name
    t.globalData[tel] = that.data.u_phone
    t.globalData[address] = that.data.u_address
    t.globalData[detail] = that.data.u_detail
    t.globalData[qu] = qu
    wx.switchTab({
      url: '/pages/index/index'
    })

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options == undefined) {
      return
    }
    var that = this;
    if (!options.qu) {
      that.setData({
        title: 1,
        u_name: t.globalData.sname,
        u_phone: t.globalData.stel == "点击填写收货信息" ? "" : t.globalData.stel,
        u_address: t.globalData.saddress == "要送到哪里？" ? "" : t.globalData.saddress,
        u_detail: t.globalData.sdetail
      })
    } else {
      that.setData({
        u_name: t.globalData.qname,
        u_phone: t.globalData.qtel == "点击填写取货信息" ? "" : t.globalData.qtel,
        u_address: t.globalData.qaddress == "从哪里取货？" ? "" : t.globalData.qaddress,
        u_detail: t.globalData.qdetail
      })
    }
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})