function a(a, t, e) {
  return t in a ? Object.defineProperty(a, t, {
    value: e,
    enumerable: !0,
    configurable: !0,
    writable: !0
  }) : a[t] = e, a;
}

var t = getApp(),
  e = require("../../utils/util.js");

Page({
  data: {
    task: 0,
    showtip: !1,
    navH: t.globalData.navHeight,
    detail: {
      status: 1
    }
  },
  onLoad: function(a) {
    var t = this,
      e = [],
      i = wx.createSelectorQuery();
    if (a.task) {
      t.setData({
        task: 1
      })
    }
    // i.select("#require").boundingClientRect(), i.select("#brands").boundingClientRect(),
    //   i.select("#product").boundingClientRect(), i.exec(function(a) {
    //     for (var t in a) e.push(a[t].top);
    //   }), t.setData({
    //     guid: a.guid,
    //     top: e
    //   }), t.getGoodsInfo();
  },
  getGoodsInfo: function() {
    var a = this,
      i = {
        guid: a.data.guid
      };
    t.api.post(i, "/api/getGoodsDetail").then(function(r) {

    })
  },
  call: function(e) {
    var phone = e.currentTarget.dataset.phone
    wx.makePhoneCall({
      phoneNumber: phone,
    })
  },
  copy: function(e) {
    var that = this,
      data = e.currentTarget.dataset.ma
    wx.setClipboardData({
      data: data,
      success: function() {
        t.api.showTips("复制成功", "success", 2e3);
      }
    })
  },
  finish: function() {
    var that = this
    if (that.data.detail.status == 2) {
      return
    }
    that.setData({
      showtip: 1
    })
  },
  zhuan: function() {
    wx.navigateBack({
      delta: 1
    })
  },
});