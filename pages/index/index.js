function t(t, a, e) {
  return a in t ? Object.defineProperty(t, a, {
    value: e,
    enumerable: !0,
    configurable: !0,
    writable: !0
  }) : t[a] = e, t;
}

var a, e = getApp(),
  i = require("../../utils/util.js");

Page({
  data: {
    showModalStatus: 1,
    currentTab: 0,
    curWeight: 0,
    cost: "",
    weight: "../../resource/images/weight_default.png",
    weight1: "../../resource/images/weight.png",
    qaddress: "",
    saddress: "",
    qtel: "",
    stel: "",
    qname: "",
    sname: "",
    qdetail: "",
    sdetail: "",
    goodsType: ["其他", "文件", "美食", "蛋糕", "手机", "鲜花"],
    banner: e.api.toArr("/uploads/miniapp/2019-11-13_1573643636_5dcbe574798fa.jpg,/uploads/miniapp/2019-11-13_1573643627_5dcbe56b962a3.jpg")
  },
  onLoad: function(t) {
    var that = this
    if (wx.getStorageSync("code") == "") {
      that.userLogin()
    }
    wx.getStorageSync("phone") == "" ? that.setData({
      showModalStatus: 1
    }) : that.setData({
      showModalStatus: 0
    })
    that.setData({
      navH: e.globalData.navHeight
    })
  },
  onShow: function() {
    var that = this,
      qname = e.globalData.qname,
      qtel = e.globalData.qtel,
      qaddress = e.globalData.qaddress,
      qdetail = e.globalData.qdetail,
      sname = e.globalData.sname,
      stel = e.globalData.stel,
      saddress = e.globalData.saddress,
      sdetail = e.globalData.sdetail
    that.setData({
      qaddress: qaddress,
      qtel: qtel,
      qdetail: qdetail,
      qname: qname,
      saddress: saddress,
      stel: stel,
      sdetail: sdetail,
      sname: sname
    })
  },
  // onShareAppMessage: function() {
  //   return {
  //     title: "免费试用 等你来选",
  //     imageUrl: "../../resource/images/shareimg.jpg",
  //     path: "/pages/index/index?talentGuid=" + wx.getStorageSync("talentGuid")
  //   };
  // },
  userLogin: function() {
    var i = this;
    wx.login({
      success: function(n) {
        console.log(n)
        wx.setStorageSync("code", n.code);
      }
    });
  },
  hideLayer: function() {
    this.setData({
      showModalStatus: 0
    });
  },
  toDetail: function(e) {
    var url,
      that = this
    e.currentTarget.dataset.qu ? url = '/pages/index/fdDetail?qu=1' : url = '/pages/index/fdDetail'
    wx.getStorageSync("phone") == "" ? that.setData({
      showModalStatus: 1
    }) : wx.navigateTo({
      url: url
    })
  },
  changeIdx: function(e) {
    var that = this,
      idx = e.currentTarget.dataset.idx
    if (idx == that.data.currentTab) {
      return
    } else {
      that.setData({
        currentTab: idx
      })
    }
  },
  changeWt: function(e) {
    var that = this
    var wt = e.currentTarget.dataset.wt
    if (wt == that.data.curWeight) {
      return
    } else {
      that.setData({
        curWeight: wt
      })
    }
  },
  changeCost: function(e) {
    var that = this
    that.setData({
      cost: e.detail.value
    })
  },
  fabu: function() {
    var that = this
    if (that.data.qaddress == "从哪里取货？") {
      e.api.showTips("请填写取货地址", "none", 3e3)
      return
    }
    if (that.data.saddress == "要送到哪里？") {
      e.api.showTips("请填写收货地址", "none", 3e3)
      return
    }
    if (that.data.cost == "" || that.data.cost == 0) {
      e.api.showTips("请填写悬赏金额", "none", 3e3)
      return
    }
    wx.showLoading({
      title: '正在发布',
      mask: true
    })
    setTimeout(() => {
      wx.hideLoading()
      e.api.showTips("发布成功！", "none", 500)
      e.globalData.qname = "",
        e.globalData.qtel = "点击填写取货信息",
        e.globalData.qaddress = "从哪里取货？",
        e.globalData.qdetail = "",
        e.globalData.sname = "",
        e.globalData.stel = "点击填写收货信息",
        e.globalData.saddress = "要送到哪里？",
        e.globalData.sdetail = ""
    }, 500)
    setTimeout(() => {
      wx.switchTab({
        url: '/pages/users/index',
      })
    }, 1000)
  },
  getPhoneNumber: function(res) {
    var that = this;
    if (res.detail.errMsg == "getPhoneNumber:ok") {
      wx.setStorageSync("phone", res.detail.encryptedData)
    } else {
      e.api.showTips("您点击了拒绝授权，将无法正常使用小程序哦~", "none", 3e3)
    }
    that.hideLayer()
  }
});