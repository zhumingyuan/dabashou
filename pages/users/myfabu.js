var t = getApp(),
  a = require("../../utils/util.js");

Page({
  data: {
    navH: t.globalData.navHeight,
    page: 1,
    list: [],
    loadtxt: "",
    task: 0
  },
  onLoad: function(e) {
    var that = this
    if (e.task) {
      that.setData({
        task: 1
      })
    }
  },
  onShow: function() {
    // this.getCollectList(1);
  },
  onPullDownRefresh: function() {
    wx.showLoading({
      title: "正在刷新",
      duration: 1e3
    }), this.getCollectList(1), wx.stopPullDownRefresh();
  },
  onReachBottom: function() {
    var t = this;
    console.log("页面滚动到底了"), t.setData({
      loadtxt: "加载中.."
    }), t.getCollectList(parseInt(t.data.page + 1));
  },
  getCollectList: function(e) {
    var i = this,
      n = {
        page: e,
        limit: 10
      };
    t.api.post(n, "/api/getUserCollects").then(function(x) {
      console.log(x)
      var n = 1 == e ? [] : i.data.list;
      for (var o in x.data.data.data) {
        var s = {
          guid: x.data.data.data[o].id,
          mainPicUrl: t.api.addBase(x.data.data.data[o].cover),
          title: x.data.data.data[o].title,
          price: x.data.data.data[o].price,
          endTime: "已截止" == a.getGoodsStatus(x.data.data.data[o].startTime, x.data.data.data[o].endTime) ? "已截止" : x.data.data.data[o].endTime
        };
        n.push(s);
      }
      i.setData({
        list: n,
        page: e,
        loadtxt: 1 != e && x.data.list.length < 1 ? "小主,已到底咯~" : ""
      });
    })
  },
  goDetail: function(t) {
    var url,
      that = this
    that.data.task ? url = "/pages/index/detail?task=1" : url = "/pages/index/detail"
    wx.navigateTo({
      url: url
    });
  }
});