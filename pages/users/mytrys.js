function t(t, a, e) {
  return a in t ? Object.defineProperty(t, a, {
    value: e,
    enumerable: !0,
    configurable: !0,
    writable: !0
  }) : t[a] = e, t;
}

var a = getApp(),
  e = require("../../utils/util.js");

Page({
  data: {
    active: 0,
    page: 1,
    loadtxt: "",
    showModalStatus: 0,
    x: a.globalData.device.windowWidth - 70,
    y: a.globalData.device.windowHeight - 110,
    tabitems: [{
      title: "综合排序",
      record: [{
          qaddress: "明发新城4幢菜鸟驿站",
          saddress: "江佑铂庭",
          cost: "2.00",
          weight: "10-15kg",
          type: "美食"
        },
        {
          qaddress: "明发新城4幢菜鸟驿站",
          saddress: "明发财富中心",
          cost: "4.00",
          weight: "小于5kg",
          type: "文件"
        }
      ]
    }, {
      title: "赏金最高",
      record: [{
          qaddress: "腾达雅苑6幢2单元菜鸟驿站",
          saddress: "虹苑新寓",
          cost: "3.00",
          weight: "10-15kg",
          type: "其他"
        },
        {
          qaddress: "腾达雅苑6幢2单元菜鸟驿站",
          saddress: "招商雍华府",
          cost: "4.00",
          weight: "5-10kg",
          type: "鲜花"
        }
      ]
    }, {
      title: "距离最近",
      record: []
    }]
  },
  onLoad: function(t) {
    var that = this
    wx.getSetting({
      success(res) {
        //若用户没有授权地理位置
        if (!res.authSetting['scope.userLocation']) {
          //在调用需授权 API 之前，提前向用户发起授权请求
          wx.authorize({
            scope: 'scope.userLocation',
            //用户同意授权
            success() {
              // 用户已经同意小程序使用地理位置，后续调用 wx.getLocation 接口不会弹窗询问
              wx.getLocation({
                success: function(res) {
                  console.log(res)
                },
              })
            },
            // 用户不同意授权
            fail() {
              wx.showModal({
                title: '提示',
                content: '此功能需获取位置信息，请授权',
                success: function(res) {
                  if (res.confirm == false) {
                    return false;
                  }
                  wx.openSetting({
                    success(res) {
                      //如果再次拒绝则返回页面并提示
                      if (!res.authSetting['scope.userLocation']) {
                        t.api.showTips("此功能需获取位置信息，请重新设置", "none", 3e3)
                      } else {
                        //允许授权，调用地图
                        that.onLoad()
                      }
                    }
                  })
                }
              })
            }
          })
        } else {
          wx.getLocation({
            success: function(res) {
              console.log(res)
            },
          })
        }
      }
    })
    that.setData({
      navH: a.globalData.navHeight
    });
  },
  // onShow: function() {
  //   var a = this;
  //   a.setData({
  //     active: 0
  //   }), this.getRecordList(this.data.page);
  // },
  onPullDownRefresh: function() {
    wx.showLoading({
      title: "正在刷新",
      duration: 1e3
    }), this.getRecordList(1), wx.stopPullDownRefresh();
  },
  onReachBottom: function() {
    var t = this;
    console.log("页面滚动到底了"), t.setData({
      loadtxt: "加载中.."
    }), t.getRecordList(parseInt(t.data.page + 1));
  },
  // onShareAppMessage: function() {
  //   return {
  //     title: "免费试用 等你来选",
  //     imageUrl: "../../resource/images/shareimg.jpg",
  //     path: "/pages/index/index?talentGuid=" + wx.getStorageSync("talentGuid")
  //   };
  // },
  onChange: function(t) {
    var a = this,
      e = a.data.tabitems;
    // for (var i in e) e[i].record = [];
    a.setData({
      active: t.detail.index,
      tabitems: e,
      page: 1,
      loadtxt: ""
    })
    // , a.getRecordList(1);
  },
  getRecordList: function(i) {
    var d = this,
      r = {
        page: i
      };
    var r, s = "tabitems[" + d.data.active + "].record",
      n = 1 == i ? [] : d.data.tabitems[d.data.active].record;
    a.api.post(r, "/api/getStock").then(function(x) {
      var r, s = "tabitems[" + d.data.active + "].record",
        n = 1 == i ? [] : d.data.tabitems[d.data.active].record;
      for (var c in x.data.data) {
        var o = {
          id: x.data.data[c].id,
          wechat: "zhongjiecom6209"
        };
        n.push(o);
      }
      d.setData((r = {}, t(r, s, n), t(r, "page", i), t(r, "times", x.data.total), t(r, "loadtxt", 1 != i && x.data.data.length < 1 ? "小主,已到底咯~" : ""),
        r));
      // if (!x.data.data || x.data.data.length < 1) {
      //   d.setData({
      //     page: d.data.page - 1
      //   })
      // }
    }, function(t) {
      a.api.getError(t).then(function(t) {}).catch();
    });
  },
  confirm: function(t) {
    var e = this;
    if (wx.getStorageSync("phone") == "") {
      e.setData({
        showModalStatus: 1
      })
      return
    }
    wx.showModal({
      title: "确认接单吗？",
      content: "重量：" + e.data.tabitems[e.data.active].record[t.currentTarget.dataset.index].weight + "\r\n价格：￥" + e.data.tabitems[e.data.active].record[t.currentTarget.dataset.index].cost + "\r\n类型：" + e.data.tabitems[e.data.active].record[t.currentTarget.dataset.index].type,
      confirmText: "确定",
      success: function(i) {
        i.confirm ? (a.api.showTips("接单成功！", "success", 300), setTimeout(() => {
          wx.navigateTo({
            url: '/pages/index/detail?task=1',
          })
        },300)) : i.cancel && console.log("用户点击取消");
      }
    });
  },
  hideLayer: function() {
    this.setData({
      showModalStatus: 0
    });
  },
  getPhoneNumber: function(res) {
    var that = this;
    if (res.detail.errMsg == "getPhoneNumber:ok") {
      wx.setStorageSync("phone", res.detail.encryptedData)
    } else {
      a.api.showTips("您点击了拒绝授权，将无法正常使用小程序哦~", "none", 3e3)
    }
    that.hideLayer()
  }
});