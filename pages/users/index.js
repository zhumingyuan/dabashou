function e(e, t, a) {
  return t in e ? Object.defineProperty(e, t, {
    value: a,
    enumerable: !0,
    configurable: !0,
    writable: !0
  }) : e[t] = a, e;
}

var t = getApp();

Page({
  data: {
    server_one: [{
      icon: "../../resource/images/myinfo.png",
      title: "我的发布",
      url: "/pages/users/myfabu"
    }, {
      icon: "../../resource/images/task.png",
      title: "我的任务",
      url: "/pages/users/myfabu?task=1"
    }],
    showModalStatus: !1,
    userInfo: wx.getStorageSync("userInfo")
  },
  onLoad: function(e) {
    wx.setNavigationBarTitle({
      title: "我的"
    }), wx.setNavigationBarColor({
      frontColor: "#ffffff",
      backgroundColor: "#019FE8"
    });
  },
  onReady: function() {},
  onShow: function() {
    var that = this;
    that.setData({
      userInfo: wx.getStorageSync("userInfo")
    });
  },
  // onShareAppMessage: function() {
  //   return {
  //     title: "免费试用 等你来选",
  //     imageUrl: "../../resource/images/shareimg.jpg",
  //     path: "/pages/index/index?talentGuid=" + wx.getStorageSync("talentGuid")
  //   };
  // },
  zhuan: function(e) {
    var a = e.currentTarget.dataset.url;
    "" == wx.getStorageSync("userInfo") ? this.showmodal() : "" == wx.getStorageSync("code") ? t.api.userLogin() : wx.navigateTo({
      url: a
    })
  },
  showmodal: function(e) {
    var t = wx.createAnimation({
      duration: 200,
      timingFunction: "linear",
      delay: 0
    });
    this.animations = t, t.translateY(300).step(), this.setData({
      animationDatas: t.export(),
      showModalStatus: !0,
      timer2: setTimeout(function() {
        t.translateY(0).step(), this.setData({
          animationDatas: t.export()
        });
      }.bind(this), 200)
    });
  },
  hideModal: function(e) {
    var t = wx.createAnimation({
      duration: 200,
      timingFunction: "linear",
      delay: 0
    });
    this.animations = t, t.translateY(300).step(), this.setData({
      animationDatas: t.export(),
      showModalStatus: !1
    });
  },
  getUserInfo: function(e) {
    var a = this;
    e.detail.userInfo ? (t.globalData.userInfo = e.detail.userInfo, a.setData({
        userInfo: e.detail.userInfo,
        hasUserInfo: !0
      }), t.api.post(e.detail.userInfo, "/api/saveInfo").then(function(e) {
        console.log(e);
      }).catch(function(e) {
        t.api.getError(e).then(function(e) {}).catch();
      }), wx.setStorageSync("userInfo", e.detail.userInfo)) : t.api.showTips("您点击了拒绝授权，将无法正常使用小程序哦~", "none", 3e3),
      a.hideModal();
  },
  getTryNum: function() {
    var a = this;
    t.api.post("", "/assessment/getAssessmentCount").then(function(t) {
      var n;
      a.setData((n = {}, e(n, "try_items[0].num", t.data.hasSubmit), e(n, "try_items[1].num", t.data.underway),
        e(n, "try_items[2].num", t.data.finished), n));
    }).catch(function(e) {
      t.api.getError(e).then(function(e) {}).catch();
    });
  }
});